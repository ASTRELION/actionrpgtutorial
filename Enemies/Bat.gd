extends KinematicBody2D

const EnemyDeathEffect = preload("res://Effects/EnemyDeathEffect.tscn")
const Stats = preload("res://Stats.gd")

enum State {
    IDLE,
    WANDER,
    CHASE
}

export var ACCELERATION = 300
export var MAX_SPEED = 50
export var FRICTION = 200

var state = State.IDLE
var velocity = Vector2.ZERO
var knockback = Vector2.ZERO
onready var stats: Stats = $Stats
onready var playerDetection = $PlayerDetection
onready var animatedSprite = $AnimatedSprite
onready var hurtbox = $Hurtbox
onready var softCollision = $SoftCollision
onready var wanderController = $WanderController
onready var animationPlayer = $AnimationPlayer

func _physics_process(delta: float) -> void:

    if (knockback != Vector2.ZERO):

        knockback = knockback.move_toward(Vector2.ZERO, FRICTION * delta)
        knockback = move_and_slide(knockback)

    match state:

        State.IDLE:

            velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
            seek_player()

            if (wanderController.get_time_left() == 0):

                state = pick_random_state([State.IDLE, State.WANDER])
                wanderController.start_wander_timer(rand_range(1, 3))

        State.WANDER:

            seek_player()

            if (wanderController.get_time_left() == 0):

                state = pick_random_state([State.IDLE, State.WANDER])
                wanderController.start_wander_timer(rand_range(1, 3))

                accelerate_towards(wanderController.targetPosition, delta)

            if (global_position.distance_to(wanderController.targetPosition) <= 4):

                state = pick_random_state([State.IDLE, State.WANDER])
                wanderController.start_wander_timer(rand_range(1, 3))

        State.CHASE:

            var player = playerDetection.player
            if (player != null):

                accelerate_towards(player.global_position, delta)

            else:

                state = State.IDLE

    if (softCollision.is_colliding()):

        velocity += softCollision.get_push_vector() * delta * ACCELERATION

    velocity = move_and_slide(velocity)


func accelerate_towards(point: Vector2, delta: float):

    velocity = velocity.move_toward(global_position.direction_to(point) * MAX_SPEED, ACCELERATION * delta)
    animatedSprite.flip_h = velocity.x < 0


func seek_player():

    if (playerDetection.can_see_player()):

        state = State.CHASE


func pick_random_state(stateList: Array):

    stateList.shuffle()
    return stateList.pop_front()


func _on_Hurtbox_area_entered(area: Area2D) -> void:

    stats.health -= area.damage
    knockback = area.knockbackVector * 100
    hurtbox.create_hit_effect()
    hurtbox.start_invincibility(0.4)


func _on_Stats_no_health() -> void:

    queue_free()
    var enemyDeathEffect = EnemyDeathEffect.instance()
    get_parent().add_child(enemyDeathEffect)
    enemyDeathEffect.global_position = global_position


func _on_Hurtbox_invincibility_ended() -> void:

    animationPlayer.play("Stop")


func _on_Hurtbox_invincibility_started() -> void:

    animationPlayer.play("Start")
