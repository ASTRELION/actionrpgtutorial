extends Area2D

signal invincibility_started
signal invincibility_ended

const HitEffect = preload("res://Effects/HitEffect.tscn")

var invincible = false setget set_invincible
onready var timer = $Timer
onready var collisionShape = $CollisionShape2D

func set_invincible(value: bool) -> void:

    invincible = value
    emit_signal("invincibility_started" if invincible else "invincibility_ended")


func start_invincibility(duration: float) -> void:

    self.invincible = true
    timer.start(duration)


func create_hit_effect() -> void:

    var effect = HitEffect.instance()
    var main = get_tree().current_scene
    main.add_child(effect)
    effect.global_position = global_position - Vector2(0, 8)


func _on_Timer_timeout() -> void:

    # Needs self so that it calls the setter
    self.invincible = false


func _on_Hurtbox_invincibility_ended() -> void:

    collisionShape.set_deferred("disabled", false)


func _on_Hurtbox_invincibility_started() -> void:

    collisionShape.set_deferred("disabled", true)
