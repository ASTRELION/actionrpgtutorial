extends Area2D

func is_colliding():

    return get_overlapping_areas().size() > 0

func get_push_vector() -> Vector2:

    var areas = get_overlapping_areas()
    var pushVector = Vector2.ZERO

    if (areas.size() > 0):

        var area = areas[0]
        pushVector = area.global_position.direction_to(global_position)

    return pushVector
