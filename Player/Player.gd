extends KinematicBody2D

const PlayerHurtSound = preload("res://Player/PlayerHurtSound.tscn")

export var MAX_SPEED = 100
export var ROLL_SPEED = 125
export var ACCELERATION = 10
export var FRICTION = 50

enum State {
    MOVE,
    ROLL,
    ATTACK
}

var velocity = Vector2.ZERO
var state = State.MOVE
var rollVector = Vector2.LEFT
var stats = PlayerStats

onready var animationPlayer = $AnimationPlayer
onready var animationTree = $AnimationTree
onready var animationState = animationTree.get("parameters/playback")
onready var blinkAnimation = $BlinkAnimation
onready var swordHitbox = $HitboxPivot/SwordHitbox
onready var hurtbox = $Hurtbox

# Called when the node is added to the scene
func _ready():

    randomize()

    stats.connect("no_health", self, "queue_free")

    animationTree.active = true
    swordHitbox.knockbackVector = rollVector


# Called every physics frame
func _process(delta):

    # Similar to a 'switch' statement
    match (state):

        State.MOVE:

            move_state(delta)

        State.ROLL:

            roll_state(delta)

        State.ATTACK:

            attack_state(delta)


func move_state(delta):

    var inputVector = Vector2.ZERO
    inputVector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
    inputVector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
    inputVector = inputVector.normalized() # Fixes speed on diag

    if (inputVector != Vector2.ZERO):

        rollVector = inputVector
        swordHitbox.knockbackVector = inputVector
        velocity = velocity.move_toward(inputVector * MAX_SPEED, ACCELERATION)
        animationTree.set("parameters/Idle/blend_position", inputVector)
        animationTree.set("parameters/Run/blend_position", inputVector)
        animationTree.set("parameters/Attack/blend_position", inputVector)
        animationTree.set("parameters/Roll/blend_position", inputVector)
        animationState.travel("Run") # changes animation state

    else:

        velocity = velocity.move_toward(Vector2.ZERO, FRICTION)
        animationState.travel("Idle")

    move()

    if (Input.is_action_just_pressed("roll")):

        state = State.ROLL

    if (Input.is_action_just_pressed("attack")):

        state = State.ATTACK


func move():

    velocity = move_and_slide(velocity)


func attack_state(inputVector):

    velocity = Vector2.ZERO
    animationState.travel("Attack")


# Called when the attack animation finishes
func attack_animation_finish():

    state = State.MOVE


func roll_state(delta):

    velocity = rollVector * ROLL_SPEED
    animationState.travel("Roll")
    move()


# Called when the roll animation finishes
func roll_animation_finish():

    velocity = Vector2.ZERO
    state = State.MOVE


func _on_Hurtbox_area_entered(area: Area2D) -> void:

    if (not hurtbox.invincible):

        stats.health -= area.damage
        hurtbox.start_invincibility(1)
        hurtbox.create_hit_effect()
        get_tree().current_scene.add_child(PlayerHurtSound.instance())


func _on_Hurtbox_invincibility_ended() -> void:

    blinkAnimation.play("Stop")


func _on_Hurtbox_invincibility_started() -> void:

    blinkAnimation.play("Start")