<div align="center">
<img src="icon.png">
</div>

# (Godot) Action RPG Tutorial

My [Godot](https://godotengine.org/) project following the tutorial by [HeartBeast](https://www.youtube.com/c/uheartbeast): https://youtube.com/watch?v=mAbG8Oi-SvQ.

[**Play Here**](https://astrelion.gitlab.io/actionrpgtutorial)

[**Assets**](https://github.com/uheartbeast/youtube-tutorials/blob/master/Action%20RPG/Action%20RPG%20Resources.zip)

![Splash](splash.png)