extends Node

export var maxHealth = 1 setget set_max_health
var health = maxHealth setget set_health

signal no_health
signal health_changed(value)
signal max_health_changed(value)

func set_health(newHealth: int) -> void:

    health = newHealth
    emit_signal("health_changed", health)

    if (health <= 0):

        emit_signal("no_health")


func set_max_health(newMaxHealth: int) -> void:

    maxHealth = newMaxHealth
    self.health = min(health, maxHealth)
    emit_signal("max_health_changed", maxHealth)


func _ready() -> void:

    self.health = maxHealth
