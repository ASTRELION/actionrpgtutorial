extends Control

var hearts = 4 setget set_hearts
var maxHearts = 4 setget set_max_hearts
onready var heartEmpty = $HeartEmpty
onready var heartFull = $HeartFull

func set_hearts(value: int) -> void:
    
    hearts = clamp(value, 0, maxHearts)
    if (heartFull):

        heartFull.rect_size.x = hearts * heartFull.texture.get_width()


func set_max_hearts(value: int) -> void:

    maxHearts = max(value, 1)
    self.hearts = min(hearts, maxHearts)
    if (heartEmpty):

        heartEmpty.rect_size.x = maxHearts * heartEmpty.texture.get_width()


func _ready() -> void:

    self.maxHearts = PlayerStats.maxHealth
    self.hearts = PlayerStats.health
    PlayerStats.connect("health_changed", self, "set_hearts")
    PlayerStats.connect("max_health_changed", self, "set_max_hearts")