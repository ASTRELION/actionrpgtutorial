shader_type canvas_item;

uniform bool active = false;

// Runs on every pixel
void fragment()
{
    vec4 previousColor = texture(TEXTURE, UV); // gets color at coordinates on the texture
    vec4 newColor = previousColor;
    if (active) newColor = vec4(1.0, 1.0, 1.0, previousColor.a);
    COLOR = newColor;
}
