extends Node2D

var Bat = preload("res://Enemies/Bat.tscn")

func _ready() -> void:

    for i in 100:

        var bat = Bat.instance()
        bat.global_position = Vector2(-64 + rand_range(-5, 5), -64 + rand_range(-5, 5))
        add_child(bat)